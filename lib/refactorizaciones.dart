void main() {
  String primerNombre = 'juan';
  String segundoNombre = 'juno';

  Iterable<Prueba> comparacionPrimerArreglo = extraerPalabra(primerNombre);

  Iterable<Prueba> comparacionSegundoArreglo = extraerPalabra(segundoNombre);

  Set<Prueba> coincidencias =
      sacarCoincidencias(comparacionPrimerArreglo, comparacionSegundoArreglo);

  List<Prueba> coincidenciasNoExactas = sacarCoindicenciaNoExacta(
      comparacionSegundoArreglo, comparacionPrimerArreglo, coincidencias);

  print('Coincidencias exactas');
  print(coincidencias);

  print('Coincidencias no exactas');
  print(coincidenciasNoExactas);
  // faltan las que no coinciden
}

List<Prueba> sacarCoindicenciaNoExacta(
    Iterable<Prueba> comparacionSegundoArreglo,
    Iterable<Prueba> comparacionPrimerArreglo,
    Set<Prueba> coincidencias) {
  List<Prueba> coincidenciasNoExactas = [];
  for (var elemento in comparacionSegundoArreglo) {
    print('elemento $elemento');
    coincidenciasNoExactas.addAll(comparacionPrimerArreglo.where((e) {
      print(e);
      return e.letra == elemento.letra && !coincidencias.contains(e);
    }));
  }
  return coincidenciasNoExactas;
}

Iterable<Prueba> extraerPalabra(String Nombre) {
  var Arreglo = Nombre.split('').asMap();
  var conjuntoArreglo = Arreglo.entries;
  var comparacionArreglo =
      conjuntoArreglo.map((e) => Prueba(letra: e.value, numero: e.key));
  return comparacionArreglo;
}

Set<Prueba> sacarCoincidencias(Iterable<Prueba> comparacionPrimerArreglo,
    Iterable<Prueba> comparacionSegundoArreglo) {
  var coincidencias = comparacionPrimerArreglo
      .toSet()
      .intersection(comparacionSegundoArreglo.toSet());
  return coincidencias;
}

class Prueba {
  final int numero;
  final String letra;

  const Prueba({required this.numero, required this.letra});
  @override
  bool operator ==(other) =>
      other is Prueba && numero == other.numero && letra == other.letra;
  @override
  int get hashCode => numero.hashCode ^ letra.hashCode;
  @override
  toString() => 'P($numero:$letra)';
}

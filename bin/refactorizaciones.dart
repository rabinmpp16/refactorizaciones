void main() {
  String primerNombre = 'juan';
  String segundoNombre = 'juno';

  var primerArreglo = primerNombre.split('').asMap();
  var segundoArreglo = segundoNombre.split('').asMap();

  var conjuntoPrimerArreglo = primerArreglo.entries;
  var comparacionPrimerArreglo =
      conjuntoPrimerArreglo.map((e) => Prueba(letra: e.value, numero: e.key));

  var conjuntoSegundoArreglo = segundoArreglo.entries;
  var comparacionSegundoArreglo =
      conjuntoSegundoArreglo.map((e) => Prueba(letra: e.value, numero: e.key));

  Set<Prueba> coincidencias =
      sacarCoincidencias(comparacionPrimerArreglo, comparacionSegundoArreglo);

  List<Prueba> coincidenciasNoExactas = [];
  for (var elemento in comparacionSegundoArreglo) {
    print('elemento $elemento');
    coincidenciasNoExactas.addAll(comparacionPrimerArreglo.where((e) {
      print(e);
      return e.letra == elemento.letra && !coincidencias.contains(e);
    }));
  }

  print('Coincidencias exactas');
  print(coincidencias);

  print('Coincidencias no exactas');
  print(coincidenciasNoExactas);
  // faltan las que no coinciden
}

Set<Prueba> sacarCoincidencias(Iterable<Prueba> comparacionPrimerArreglo,
    Iterable<Prueba> comparacionSegundoArreglo) {
  var coincidencias = comparacionPrimerArreglo
      .toSet()
      .intersection(comparacionSegundoArreglo.toSet());
  return coincidencias;
}

class Prueba {
  final int numero;
  final String letra;

  const Prueba({required this.numero, required this.letra});
  @override
  bool operator ==(other) =>
      other is Prueba && numero == other.numero && letra == other.letra;
  @override
  int get hashCode => numero.hashCode ^ letra.hashCode;
  @override
  toString() => 'P($numero:$letra)';
}

import 'package:refactorizaciones/refactorizaciones.dart';
import 'package:test/test.dart';

void main() {
  group('comparaciones', () {
    test('si se compara juan y juno deben coincidir "J" y "U" ', () {
      var primeraPalabra = extraerPalabra("juan");
      var segundaPalabra = extraerPalabra("juno");
      List<Prueba> x = [];
      x.add(Prueba(numero: 0, letra: "j"));
      x.add(Prueba(numero: 1, letra: "u"));
      expect(sacarCoincidencias(primeraPalabra, segundaPalabra), x.toSet());
    });

    test('si se compara juan y jias deben coincidir "J" "a"', () {
      var primeraPalabra = extraerPalabra("juan");
      var segundaPalabra = extraerPalabra("jias");
      List<Prueba> x = [];
      x.add(Prueba(numero: 0, letra: "j"));
      x.add(Prueba(numero: 2, letra: "a"));
      expect(sacarCoincidencias(primeraPalabra, segundaPalabra), x.toSet());
    });
    test('si se compara juan y joen deben coincidir "J" "n"', () {
      var primeraPalabra = extraerPalabra("juan");
      var segundaPalabra = extraerPalabra("joen");
      List<Prueba> x = [];
      x.add(Prueba(numero: 0, letra: "j"));
      x.add(Prueba(numero: 3, letra: "n"));
      expect(sacarCoincidencias(primeraPalabra, segundaPalabra), x.toSet());
    });
  });
}
